echo "################## NEW LOG ##################" >> /home/pi/startup.log &
echo "$(date) [bash_profile]: Starting..." >> /home/pi/startup.log &
echo "$(date) [bash_profile]: Loading .bashrc" >> /home/pi/startup.log &
# Load .bashrc "manually" it's not loaded by .profile if .bash_profile exists
if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi

echo "$(date) [bash_profile]: Starting X..." >> /home/pi/startup.log &
# https://die-antwort.eu/techblog/2017-12-setup-raspberry-pi-for-kiosk-mode/
# Because we already configured the Pi to autologin the pi user, we can use its .bash_profile for starting X. Simply append the following line:
# [[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && startx -- -nocursor
[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && startx

echo "$(date) [bash_profile]: All done" >> /home/pi/startup.log &
