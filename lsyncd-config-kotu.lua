-- https://axkibe.github.io/lsyncd/manual/config/file/

settings {
  logfile = "log/lsyncd.log",
  statusFile = "log/lsyncd-status.log",
  -- insist = true
  nodaemon = true,
}

sync {
  default.rsyncssh,
  delay = 1,
  source = "/home/croc/work/git/kotu",
  -- host = "pi@192.168.0.162",
  host = "pi@192.168.0.158",
  exclude = { 'log', '.*', 'build', '*.iml', 'gradle' },
  targetdir = "work/git/kotu",

  rsync = {
--     archive = true,
--     compress = false,
     whole_file = true
   },

  ssh = {
    identityFile = "~/.ssh/rpi"
  }
}
