alias ll='ls -l'
alias 1wire='cat /sys/bus/w1/devices/28-0516a4efbeff/w1_slave'
alias ango-copyStatic='cp -r ~/work/git/ango/static ~/work/git/kotu/static'
alias supgrade='sudo apt update && sudo apt upgrade && sudo apt autoremove'
alias cp-kotu-static='cp -r ~/work/git/kotu/static ~/IdeaProjects/kotu'

alias scrls='screen -ls'
alias scr-kotu='cd ~/work/git/kotu && screen -R kotu'
alias scr-orientDB='cd ~/db/orientDB/orient*/bin && screen -R orientDB'
alias scr-chromium='export DISPLAY=:0 && screen -R chromium'

alias start-kotu-dev='scr-kotu ./gradlew -i --no-daemon bootRun'
alias start-kotu-jar='scr-kotu java -jar jar/kotu-0.1-SNAPSHOT.jar'
alias start-kotu=start-kotu-jar
# alias restart-kotu='screen -S kotu -X quit && start-kotu'

# alias start-orientDB='scr-orientDB ./server.sh'
alias screenOn='sudo sh -c "echo 0 > /sys/class/backlight/rpi_backlight/bl_power"'
alias screenOff='sudo sh -c "echo 1 > /sys/class/backlight/rpi_backlight/bl_power"'

function applyRpi {
  set -x # enable commands logging

  sudo cp $HOME/work/git/rpi/autostart.openbox /etc/xdg/openbox/autostart
  sudo chmod +x /etc/xdg/openbox/autostart

  cp $HOME/work/git/rpi/bash_aliases $HOME/.bash_aliases
  cp $HOME/work/git/rpi/bash_profile $HOME/.bash_profile

  set +x # disable commands logging

  source $HOME/.bash_aliases
  source $HOME/.bash_profile
}

function restartKotu {
  set -x # enable commands logging
  screen -S kotu -X quit
  killall java
  start-kotu
  set +x # disable commands logging
}

function logRPi() {
  echo "$(date) [$USER@$0:$$]: $*" >> /home/pi/startup.log
}
