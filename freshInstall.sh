#!/bin/sh -x

#####
# How to use 'sed' command
# https://www.computerhope.com/unix/used.htm
# https://linuxconfig.org/learning-linux-commands-sed
#####

$?

checkExitCode() {
  # [ $? -eq 0 ] || (echo "$1" && exit $?)
  if [ $? -ne 0 ]; then
  # if [[ $? != 0 ]]; then
    echo "$1"
    set +x # disable commands logging
    exit $?
  fi
}

read -p "Enable hardware DS3231 RTC (Real Time Clock)? [y/n]: " yesRTC
if [ $yesRTC = "y" ]; then
  yesRTC = "y"
fi

read -p "Install EVERYTING without your intervention? [y/n]: " yesAll
if [ -z $yesAll ] || [ $yesAll = "y" ]; then
  yesAll = "y"
  input = "y"
fi

if [ $yesAll = "n" ]; then
  read -p "Install all the packages without your intervention? [y/n]: " input
fi
if [ -z $input ] || [ $input = "y" ]; then
  noAsk="-y"
  echo "OK, '-y' switch will be used"
else
  noAsk=""
  echo "OK, you will be asked for every apt install/remove to be confirmed by you."
fi

# create .ssh dir because it might be needed later
mkdir ~/.ssh
# enable X11 forwarding for easier access
sudo sed -i.orig 's/#X11Forwarding .*/X11Forwarding yes/' /etc/ssh/sshd_config
checkExitCode "Couldn't enable X11 forwarding"

#########
# Set Swap to 2 GB
# https://www.bitpi.co/2015/02/11/how-to-change-raspberry-pis-swapfile-size-on-rasbian/
#########
echo
echo "### Increasing Swap to 2 GB"
free -m
# find CONF_SWAPSIZE and replace whatever size it was set (usually 100) with 2048
sudo sed -i.orig "s/\(CONF_SWAPSIZE\)\(=[0-9]\+\)/\1=2048/" /etc/dphys-swapfile
echo "### Restarting swapfile"
sudo /etc/init.d/dphys-swapfile stop
sudo /etc/init.d/dphys-swapfile start
echo "### Swap should now be 2 GB"
sudo free -m
checkExitCode "Couldn't increase swapfile"
#########

configFile="/boot/config.txt"
cmdFile="/boot/cmdline.txt"

echo
echo "### Disabling overscan"
sudo sed -i.orig 's/#disable_overscan=1/disable_overscan=1/g' $configFile
# could also use 's/#\(disable_overscan\).*/\1=1/g' -- find "disable_overscan", keep that (drop the "#") and append "=1"
# or use 's/.*disable_overscan=.\+/disable_overscan=1/g' -- find a line that contains "disable_overscan=", and replace the *whole* line with "disable_overscan=1"
echo
echo "### Enabling hardware interfaces:"
echo " - I2C"
sudo sed -i 's/#dtparam=i2c_arm=on/dtparam=i2c_arm=on/g' $configFile
echo " - SPI"
sudo sed -i 's/#dtparam=spi=on/dtparam=spi=on/g' $configFile
echo " - 1-wire"
# add if doesn't exist
grep "dtoverlay=w1-gpio" $configFile || sudo sed -i "$ a dtoverlay=w1-gpio" $configFile
echo " - Enabling LCD"
grep "ignore_lcd=0" $configFile || sudo sed -i "$ a ignore_lcd=0" $configFile
echo
if [ $yesAll = "n" ]; then
  read -p "Rotate LCD screen by 180 degrees? [y/n]: " input
fi
if [ -z $input ] || [ $input = "y" ]; then
  grep "lcd_rotate=2" $configFile || sudo sed -i "$ a lcd_rotate=2" $configFile
fi
checkExitCode "Couldn't set configFile params"

#####
# Adding a Real Time Clock to Raspberry Pi
#
# https://learn.adafruit.com/adding-a-real-time-clock-to-raspberry-pi/set-rtc-time
#
if [ $yesRTC = "y" ]; then
  echo "Enabling hardware RTC"
  grep "dtoverlay=i2c-rtc,ds3231" $configFile || sudo sed -i "$ a dtoverlay=i2c-rtc,ds3231" $configFile

  echo "Removing fake hardware clock"
  sudo apt-get -y remove fake-hwclock
  sudo update-rc.d -f fake-hwclock remove
  sudo systemctl disable fake-hwclock

  hwClockFile="/lib/udev/hwclock-set"
  # find and replace pattern
  fNr="if \[ -e \/run\/systemd\/system \] ; then\n\s*exit 0\nfi"

  grep "#if \[ -e /run/systemd/system \]" $hwClockFile || sudo perl -0777 -i.orig -pe "s/${fNr}/#if \[ -e \/run\/systemd\/system \] ; then\n#  exit 0\n#fi/g" $hwClockFile

  ### this one should take care of both cases (the one with '--badyear' at the end and the one without)
  fNr='\/sbin\/hwclock --rtc=$dev --systz'
  grep "#${fNr}" $hwClockFile || sudo sed -i "s/${fNr}/#${fNr}/g" $hwClockFile

  echo "Checking clock"
  sudo hwclock -r --verbose
  echo "Setting clock"
  sudo hwclock -w
  echo "Clock set to:"
  sudo hwclock -r

  checkExitCode "Couldn't enable hardware RTC"
fi

# # first add custom PPAs so we do 'sudo apt update' only once
# echo
# echo "### Adding Oracle Java 8 PPA"
# sudo apt-get install $noAsk dirmngr --install-recommends
# # echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu artful main" | sudo tee /etc/apt/sources.list.d/webupd8team-java.list
# # echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu artful main" | sudo tee -a /etc/apt/sources.list.d/webupd8team-java.list
# echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | sudo tee /etc/apt/sources.list.d/webupd8team-java.list
# echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | sudo tee -a /etc/apt/sources.list.d/webupd8team-java.listsudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
# echo "If you have problems installing Java 8 on Pi, see this link: https://stackoverflow.com/questions/46815897/jdk-8-is-not-installed-error-404-not-found"
# echo

echo "### Updating packages"
sudo apt update $noAsk
sudo apt full-upgrade $noAsk
sudo apt autoremove $noAsk
echo

# WiringPi - https://github.com/Pi4J/pi4j/issues/411
echo "### Installing WiringPi lib (for Pi4J)"
sudo apt install $noAsk wiringpi
### PI GPIO doesn't seem to be needed atm
# PI GPIO issue - https://www.raspberrypi.org/forums/viewtopic.php?t=176712
# echo "### Installing PI GPIO service"
# sudo apt install $noAsk pigpio
# echo "### Enabling PI GPIO service"
# sudo systemctl enable pigpiod
# echo "### Checking status of PI GPIO service:"
# sudo systemctl status pigpiod
checkExitCode "Couldn't install WiringPi"
echo

#########
# SETTING UP KIOSK PART
#########
# https://die-antwort.eu/techblog/2017-12-setup-raspberry-pi-for-kiosk-mode/
#########
# Set up basic graphical environment with just:
# 1.) X server (usually X.Org)
# 2.) Window manager (Openbox, XFWM, …)
echo "### Installing XOrg server"
sudo apt-get install $noAsk --no-install-recommends xserver-xorg x11-xserver-utils xinit openbox $1
checkExitCode "Couldn't install XOrg server"
echo
echo "### Configuring Openbox"
sudo mv /etc/xdg/openbox/autostart /etc/xdg/openbox/autostart.old
sudo cp autostart.openbox /etc/xdg/openbox/autostart
sudo chmod +rx /etc/xdg/openbox/autostart
checkExitCode "Couldn't configure Openbox"
echo
#echo "Installing Raspberry Pi Desktop"
#apt-get install $noAsk --no-install-recommends raspberrypi-ui-mods lxterminal gvfs $1
#echo
echo "### Installing chromium-browser"
sudo apt-get install $noAsk --no-install-recommends chromium-browser
checkExitCode "Couldn't install chromium-browser"
# echo "### Installing Midori browser"
# sudo apt-get install $noAsk --no-install-recommends midori

cp bash_aliases ~/.bash_aliases
cp bash_profile ~/.bash_profile
checkExitCode "Couldn't copy bash scripts"
#########

#########
# Setting up custom splash screen
# https://www.yingtongli.me/blog/2016/12/21/splash.html
# https://www.raspberrypi.org/forums/viewtopic.php?p=1326448#p1335189
#########
echo
echo "### Disabling rainbow splash screen"
grep "disable_splash=1" $configFile || sudo sed -i "$ a disable_splash=1" $configFile
echo
echo "### Disabling the Raspberry Pi logo in the corner of the screen and various bits of output from the kernel and friends"
sudo sed -i.orig "s/console=tty1/console=tty3/" $cmdFile
# add if doesn't exist
grep "vt.global_cursor_default=0 logo.nologo consoleblank=0 loglevel=1 splash quiet" $cmdFile || sed -i "s/$/ vt.global_cursor_default=0 logo.nologo consoleblank=0 loglevel=1 splash quiet &/" $cmdFile
# sudo sed -i.orig "s/$/ logo.nologo consoleblank=0 loglevel=1 quiet/" $cmdFile
checkExitCode "Couldn't configure splash screen"
echo
# echo "### Disable the login prompt"
# sudo systemctl disable getty@tty1
echo
echo "### Installing framebuffer image viewer"
sudo apt install -y fbi
checkExitCode "Couldn't install fbi"
echo
echo "### Enabling the service to be run at boot"
sudo systemctl enable splashscreen
checkExitCode "Couldn't enable splashscreen service"
echo
echo "### Copying splash screen"
sudo cp splashscreen.service /etc/systemd/system/splashscreen.service
sudo cp splashscreen/splash.png /opt/splash.png
checkExitCode "Couldn't copy splash screen"
echo
echo "### Installing feh and setting openbox background"
sudo apt install -y feh
checkExitCode "Couldn't install feh"
### this only works in a running X session (desktop environment)
# sudo feh --bg-fill /opt/splash.png
# checkExitCode "Couldn't set feh background"
#########

#########
# Install essential programs/tools
#########
echo "### Installing LSOF (for htop files list view) ###"
sudo apt install $noAsk lsof
checkExitCode "Couldn't install lsof"
echo
echo "### Installing GIT ###"
sudo apt install $noAsk git
checkExitCode "Couldn't install git"
echo
echo "### Installing Screen ###"
sudo apt install $noAsk screen
checkExitCode "Couldn't install screen"
echo
#echo "Installing Raspbian GUI config"
#apt install $noAsk rc-gui
#echo
# https://www.raspberrypi.org/forums/viewtopic.php?t=101543#p1220629

##################
# IF YOU HAVE PROBLEMS INSTALLING JAVA 8 SEE LINK BELOW:
# https://stackoverflow.com/questions/46815897/jdk-8-is-not-installed-error-404-not-found
# ---
# Make sure you have the correct version of java (8u201 in the example below) and then run this:
#
# cd /var/lib/dpkg/info
# sudo sed -i 's|JAVA_VERSION=8u191|JAVA_VERSION=8u201|' oracle-java8-installer.*
# sudo sed -i 's|PARTNER_URL=http://download.oracle.com/otn-pub/java/jdk/8u191-b12/2787e4a523244c269598db4e85c51e0c/|PARTNER_URL=http://download.oracle.com/otn-pub/java/jdk/8u201-b09/42970487e3af4f5aa5bca3f542482c60/|' oracle-java8-installer.*
# sudo sed -i 's|SHA256SUM_TGZ=.*|SHA256SUM_TGZ="5103b9e33ac3aa353f06b11976a496b4cc09dd6d6614c5cc0ec2693a12decb78"|' oracle-java8-installer.*
# sudo sed -i 's|J_DIR=jdk1.8.0_191|J_DIR=jdk1.8.0_201|' oracle-java8-installer.*
#
# You should also check for the correct SHA256SUM_TGZ for jdk-8u201-linux-arm32-vfp-hflt.tar.gz on: https://www.oracle.com/webfolder/s/digest/8u201checksum.html
# Again, make sure you use the correct link for your version of Java (replace "8u201" in the link with what's the latest version at the time)!
##################
# echo "### Installing Java 8 ###"
echo "### Installing OpenJDK 11 ###"
# echo "If you have problems installing Java 8 on Pi, see this link: https://stackoverflow.com/questions/46815897/jdk-8-is-not-installed-error-404-not-found"
# sudo apt-get purge $noAsk openjdk*
# sudo apt install $noAsk oracle-java8-installer
# sudo apt install $noAsk oracle-java8-set-default
sudo apt install $noAsk openjdk-11-jre-headless
# sudo update-alternatives --config java
# echo "### Java 8 installed ###"
checkExitCode "Couldn't install Java"
echo "### OpenJDK 11 installed ###"
echo
#########

#########
# Download and install OrientDB
#########
# dbVersion="3.0.10"
# read -p "Install OrientDB $dbVersion? [y/n]: " input
# if [ $input == "y" ]; then
#   echo "### Installing OrientDB version $dbVersion"
#   cd ~/Downloads
#   dbLink="https://s3.us-east-2.amazonaws.com/orientdb3/releases/$dbVersion/orientdb-$dbVersion.tar.gz"
#   echo "Downloading OrientDB from: $dbLink"
#   wget $dbLink
#   dbPath="$HOME/db/orientDB/"
#   mkdir -p $dbPath
#   echo "Extracting OrientDB to $dbPath"
#   tar -zvxf orientdb-${dbVersion}.tar.gz -C $dbPath
#   echo "Patching server.sh"
#   cd ${dbPath}orientdb-${dbVersion}/bin
#   echo "Removing Java 64 bit run flag"
#   sed -i.orig 's/ -d64 / /g' server.sh # also makes a backup into a *.orig file
#   memLimit="128m"
#   echo "Lowering memory limits to: $memLimit"
#   sed -i "s/-XX:MaxDirectMemorySize=512g/-XX:MaxDirectMemorySize=$memLimit/g" server.sh # v2.x
#   sed -i "s/-Xms\(\w\)\+/-Xms$memLimit/" server.sh
#   sed -i "s/-Xmx\(\w\)\+/-Xmx$memLimit/" server.sh
#
#   echo -e '\e[31m!!! Next stuff is for DEBUGGING only !!!\e[0m'
#
#   echo "Adding default \"admin\" username and password to orientdb-server-config.xml"
#   cd ../config
#   sed -i.orig 's/<users>/<users>\
#           <user resources=\"*\" password=\"{PBKDF2WithHmacSHA256}627F793B5A429F07B828F7F9E685CFC895FF094D4BB98E9F:AA1B5ACE27E7FA2B5802676EF5B4F34E91FC9263CC93E10A:65536\" name=\"root\"\/>\
#           <user resources=\"connect,server.listDatabases,server.dblist\" password=\"{PBKDF2WithHmacSHA256}D5DBB2A57D193706B9C05F6799D5C6D4396C58A0ED8975FF:39E602BC7CAB90A4551A0995B6AF827A35CB278FD4D30A9B:65536\" name=\"guest\"\/>/' orientdb-server-config.xml
#
#   echo "Increasing session expire timeout to 8 hours"
#   sed -i 's/<\/properties>/    <entry value="28800" name="network.http.sessionExpireTimeout"\/>\
#   <\/properties>/' orientdb-server-config.xml
# fi
#########

#########
# Download Kotu and Ango
#########
echo "### Getting Git repos (Kotu and Ango)"
cd ~
mkdir -p work/git
cd work/git/
if [ $yesAll = "n" ]; then
# read -p "Download dependencies and build Kotu? [y/n]: " input
  read -p "Download Kotu? [y/n]: " input
fi
if [ -z $input ] || [ $input = "y" ]; then
  echo "### Getting Kotu"
  git clone git@gitlab.com:crocx/kotu.git
  # echo "Building Kotu - this will take a while..."
  # cd kotu
  # ./gradlew build
fi
echo

if [ $yesAll = "n" ]; then
  read -p "Download Ango? [y/n]: " input
fi
if [ -z $input ] || [ $input = "y" ]; then
  echo "### Getting Ango"
  cd ~/work/git
  git clone git@gitlab.com:crocx/ango.git
fi
echo
#########

#########
# Install PostgreSQL 9.6
# https://bpwalters.com/blog/setting-up-postgresql-on-raspberry-pi/
# https://opensource.com/article/17/10/set-postgres-database-your-raspberry-pi
#########
pgVersion="11"
if [ $yesAll = "n" ]; then
  read -p "Install PostgreSQL $pgVersion? [y/n]: " input
fi
if [ -z $input ] || [ $input = "y" ]; then
  # sudo apt-get install -y --no-install-recommends postgresql-${pgVersion}
  sudo apt-get install -y postgresql-${pgVersion}
  checkExitCode "Couldn't install PostgreSQL"
fi

if [ $yesAll = "n" ]; then
  read -p "Open PostgreSQL to remote access? [y/n]: " input
fi
if [ -z $input ] || [ $input = "y" ]; then
  pgPath="/etc/postgresql/${pgVersion}/main"
  sudo sed -i.orig "s/.*listen_addresses.*/listen_addresses = '*'/" ${pgPath}/postgresql.conf
  echo "Opening PostgreSQL to remote access from local network: 192.168.0.0/24"
  # sudo cp -b ${pgPath}/pg_hba.conf ${pgPath}/pg_hba.conf.orig
  sudo grep "host     all     all     192.168.0.0/24     md5" ${pgPath}/pg_hba.conf || sudo sed -i.orig "$ a host     all     all     192.168.0.0/24     md5" ${pgPath}/pg_hba.conf
  # echo "Opening PostgreSQL to remote access from global network: 0.0.0.0/24"
  # sudo grep "host     all     all     0.0.0.0/24     md5" ${pgPath}/pg_hba.conf || sed -i "$ a host     all     all     0.0.0.0/24     md5" >> ${pgPath}/pg_hba.conf
  sudo systemctl restart postgresql

### http://www.bashguru.com/2010/01/shell-colors-colorizing-shell-scripts.html # how to color bash output
  echo -e '\e[31m!!! You have to set a password to be able to connect to DB remotely !!!\e[0m'
  echo -e 'When you get to DB console (noted by DB version on top and "postgres=#" console prompt),
write "\e[33m\password postgres\e[0m" and enter the new DB password. When you are done write "\e[33m\q\e[0m" to quit.'
  sudo -u postgres psql
  checkExitCode "Couldn't open PostgreSQL to remote access"
fi
#########

#########
# Reboot
#########
if [ $yesAll = "n" ]; then
  read -p "All done, reboot? [y/n]: " input
fi
if [ -z $input ] || [ $input = "y" ]; then
  echo "OK, cya!"
  sudo reboot
else
  echo "Aait, do it yourself then. Cheers!"
  exit
fi
