##!/bin/sh -x

piIP=$1
if [ -z "$piIP" ]; then
  piIP="raspberrypi"
  echo "No Raspberry Pi IP specified, using default '${piIP}', but may not work."
elif [ "${#piIP}" -lt 4 ]; then
  echo "Interpreting short IP '${piIP}' as last IP number."
  piIP="192.168.0.${piIP}"
fi

echo "Using IP: '${piIP}'"

pubKey=$2
if [ -z "$pubKey" ]; then
  pubKey="$HOME/.ssh/rpi.pub"
  echo "No user public key specified, using default '${pubKey}'."
fi

echo "Using public key: ${pubKey}"

read -s -p "Enter password for your Pi (leave blank for default): " piPass
if [ -z "$piPass" ]; then
  echo "Using default password."
  piPass="raspberry"
fi

# scp ${pubKey} pi@${piIP}:~/.ssh/authorized_keys
# ssh-copy-id -i ${pubKey} pi@${piIP}
sshpass -p "$piPass" ssh-copy-id -i ${pubKey} pi@${piIP}

if [ $? != 0 ]; then
  echo "Could not copy public key to Pi!"
  exit
fi

echo "Public key copied."

privateKey=$3
if [ -z "$privateKey" ]; then
  pubKey="$HOME/.ssh/rpi"
  echo "No user private key specified, using default '${privateKey}'."
fi

ssh -i ${privateKey} pi@${piIP} "mkdir -p ~/work/git"
echo "Created folder '~/work/git' for further work"
echo "You should be able to run lsyncd-rpi ('lsyncd ~/work/git/rpi/lsyncd-config-rpi.lua') for further file syncing..."
