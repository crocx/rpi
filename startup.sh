#!/bin/sh -x

echo "$(date) [startup.sh]: Starting..." >> /home/pi/startup.log

echo "$(date) [startup.sh]: Setting display stuff" >> /home/pi/startup.log
# Disable any form of screen saver / screen blanking / power management
xset s noblank # don't blank the video device
xset s off # disable screen saver
xset s 0 0
# xset s noexpose
xset dpms 180 180 180 # turn screen off after n seconds
xset +dpms # enable DPMS (Energy Star) features.
xset q >> /home/pi/startup.log
# xset -dpms # disable DPMS (Energy Star) features.

export DISPLAY=:0

# Allow quitting the X server with CTRL-ATL-Backspace
# setxkbmap -option terminate:ctrl_alt_bksp

echo "$(date) [startup.sh]: Setting background fill" >> /home/pi/startup.log
### this only works in a running X session (desktop environment)
sudo feh --bg-fill /opt/splash.png

# echo "Sleeping for 10 seconds to give DB enough time to start up..."
# sleep 10
echo "$(date) [startup.sh]: Starting Kotu in the background" >> /home/pi/startup.log
echo "Starting Kotu in background (screen)"
cd ~/work/git/kotu
# screen -dmS kotu ./gradlew -i --no-daemon bootRun >> /home/pi/startup.log
screen -dmS kotu java -jar jar/kotu-0.1-SNAPSHOT.jar >> /home/pi/startup.log

echo "$(date) [startup.sh]: Starting chromium" >> /home/pi/startup.log
### Start Chromium in kiosk mode
# https://peter.sh/experiments/chromium-command-line-switches
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' ~/.config/chromium/'Local State'
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/; s/"exit_type":"[^"]\+"/"exit_type":"Normal"/' ~/.config/chromium/Default/Preferences
# chromium-browser --disable-infobars --kiosk 'http://localhost:3000'
# chromium-browser 'http://localhost:3000' --pull-to-refresh --single-process --renderer-process-limit=1 --mem-pressure-system-reserved-kb=850000 --enable-low-end-device-mode --enable-offline-auto-reload --disable-infobars --start-fullscreen
# chromium-browser 'http://localhost:3000' --pull-to-refresh --renderer-process-limit=1 --mem-pressure-system-reserved-kb=850000 --enable-low-end-device-mode --enable-offline-auto-reload --disable-infobars --start-fullscreen
chromium-browser 'http://localhost:3000' --pull-to-refresh --enable-low-end-device-mode --enable-offline-auto-reload --disable-infobars --start-fullscreen

### Start Midori in kiosk mode
# https://maker-tutorials.com/en/auto-start-midori-browser-in-fullscreen-kiosk-modus-raspberry-pi-linux/
# http://midori-browser.org/faqs/
# midori -e Fullscreen -a http://localhost:3000

echo "$(date) [startup.sh]: All done" >> /home/pi/startup.log
