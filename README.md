# Instructions #

Here are instructions on how to set up a fresh install of
[*Raspbian* **Lite**](https://www.raspberrypi.org/downloads/raspbian/) to run
Kotu and Ango in kiosk mode.


## On first run after a clean install ##

1. First you should enable SSH so you can do everything else on your Pi via SSH.
   - One option is to create a plain `ssh` file (no file ending) on the main/boot
     sector of the drive/SD card.
   - The other option is to connect a keyboard to the Pi and go to Pi's console
     and run `sudo raspi-config`. Go to `5 Interfacing Options` and
     enable `P2 SSH` (you can also enable `P3 VNC` while you're there if you think
     you'll be connecting to your Pi via VNC). After enabling SSH you should restart
     your Pi and connect to it via SSH (e.g. `ssh pi@raspberrypi` or use your Pi's
     actual IP if `raspberrypi` can't get resolved - default password for SSH login
     is `raspberry`).

     - After reboot you can make sure the filesystem was expanded to cover the
       whole SD card. To do so you can/check that via `sudo raspi-config` and then under `7 Advanced Options` -> `A1 Expand filesystem`. This should happen on first boot so it's probably not needed, but you can check it out just in case.

  - Optionally go to `3 Boot Options` -> `B1 Desktop / CLI` and select
     `B2 Console Autologin` if the system doesn't log the default `pi` user in
     automatically on system start.

  1.1. Run `firstConnection.sh` to copy your public keys to Pi for easier access
    (SSH-ing to Pi without a password). You need to have `sshpass` installed (`sudo apt install sshpass`).

  1.2. Enable 1-wire via `sudo raspi-config` -> `5 Interfacing Options` to add
     support for 1-wire sensors.

2. If every went well, you can use `lsyncd` to make syncing/copying of files easier (see instructions "*Using
  `lsyncd` for easier continuous file syncing*" below). Run `lsyncd-rpi` (`alias lsync-rpi='mkdir -p ~/rpi/log && cd ~/rpi && lsyncd ~/work/git/rpi/lsyncd-config-rpi.lua'`) and if files are copied successfully, proceed to step 3.

   - You can also copy `freshInstall.sh`, `autostart.openbox` and `startup.sh` to user `pi`'s
   `home` directory under `rpi` (`~/work/git/rpi`) manually. You can use `scp` for that (e.g.
   `scp -r work/git/rpi/ pi@raspberrypi:~` etc.).

3. Go to `~/work/git/rpi` and make sure `freshInstall.sh` is runnable (or just make it
   so with `chmod +x freshInstall.sh`) and run it (via `./freshInstall.sh`).
   This should install everything needed to start using Ango and Kotu on your Pi.

   - Make sure `.bash_profile` and `.bash_aliases` are in the `pi`'s user `home/pi`
   directory (`~`). They should be copied with the `freshInstall.sh` script.

   - Also make sure overscan is disabled (under `7 Advanced options` ->
      `A2 Overscan`) or that it was disabled by running `freshInstall.sh`.

4. Before you run Kotu, make sure GPIO pins are accessible in non-privileged mode.
   Run `sudo raspi-config`, go to `5 Interfacing Options` and enable `P2 SSH`

-----

# Using SSH keys for easier access

You can also [generate SSH keys](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2)
and move them to Pi for easier SSH and SCP login.

1. Create `~/.ssh` directories on Pi and your computer if they don't exist already.
  - If you don't want to type in the password every time you want to log in
    Pi via SSH/SCP, generate the keys without the passphrase (though that
    is a security risk).
2. Go to `~/.ssh` and run `ssh-keygen -t rsa` on your computer to generate the
   SSH keys. You'll be asked where to save the generated key - you can just
   type `rpi` for instance.
3. Two keys will be generated (where you specified). Append the public key
   (eg. `rpi.pub`) to Pi's `~/.ssh/authorized_keys` **file** (not directory) with
   running `firstConnection.sh`.
   - You could do it maunally as well by running
     `scp rpi.pub pi@raspberrypi:~/.ssh/authorized_keys` (or instead of
     `pi@raspberrypi` use your Pi's IP address as `pi@XXX.XXX.XXX.XXX` if
     `raspberrypi` can't be resolved), but make sure the content of `rpi.pub`
     gets copied into a `authorized_keys` file, and not the `authorized_keys` folder!
4. Now you can SSH to Pi with `ssh -i ~/.ssh/rpi.pub pi@raspberrypi` or SCP
   with `scp -i ~/.ssh/rpi.pub <file> pi@raspberrypi` using your SSH keys.
   - It's usefull to make such commands into aliases or functions and put them
     in your `~/.bash_aliases` file.

# Using `lsyncd` for easier continuous file syncing

1. Install `lsyncd` (`sudo apt install lsyncd`) on your host/workstation
  computer where you wish to continuously sync files from (to your Pi).
  - Make sure `rsync` is installed on your machine and Pi as well (should be by
    default).
2. Make a config scrip like `lsyncd-config-rpi.lua` (file ending doesn't have to
   be `.lua`, but it's easier to work with such files).
   - Make sure you configure the absolute source (under `source`) and destination
     (under `targetdir`; empty dir starts at a remote location home ie. `~`),
     paths to match your working environment
3. Run `lsyncd` with `lsyncd ~/rpi/lsyncd-config-rpi.lua` for example.
  - Make sure log dir exists (if it's specified in the config file) before
    running `lsyncd` beacuse it's not created automatically.

# Using X11 forwarding for easier access

You can also use X11 forwarding when using SSH for easier file manipulation/editing
on your Pi. Though this requires that the server and the client both have the
same program installed.

See [How to forward X over SSH to run graphics applications remotely?](https://unix.stackexchange.com/questions/12755/how-to-forward-x-over-ssh-to-run-graphics-applications-remotely#12772) for some help/reference.

1. First make sure X11 forwarding is enabled on your server (it usually is). Check
   that `/etc/ssh/sshd_config` on your server/Pi contains:
```
X11Forwarding yes
```
  - Also make sure you have `xauth` installed on your server/Pi.
  - If needed you can restart the SSH service on your server/Pi with
    `sudo service ssh restart`

2. Connect to your server/Pi with `ssh -X pi@raspberrypi` (note the `-X` param).
   Once you're logged in you can start `leafpad` on your Pi (via SSH but the same
   program has to be installed on your computer/client as well) for instance
   and the program should show/open up on your computer/client (not on the Pi)
   and you can then edit files on your Pi with `leafpad` like they were on your
   computer.

-----

# Notes #

- You can move `[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && startx -- -nocursor`
  line from `~/.bash_profile` to the end of `~/.profile` file if you are using
  other shells than Bash, but then you have to remove `~/.bash_profile` if you
  want `~/.profile` to be read on startup.

- If your Pi can't connect to your WiFi with a hidden SSID, make sure to include
  the `scan_ssid=1` parameter in your configuration.
  Example of `/etc/wpa_supplicant/wpa_supplicant.conf`:
```
network={
        ssid="my hidden ssid"
        scan_ssid=1
        psk="your secret password with spaces"
}
```

- [Adjusting the Brightness of the Official Touchscreen Display](https://raspberrypi.stackexchange.com/questions/46225/adjusting-the-brightness-of-the-official-touchscreen-display)

  You can turn the official RPi display on/off with `echo 0 > /sys/class/backlight/rpi_backlight/bl_power`
  or change it's brightness with `echo n > /sys/class/backlight/rpi_backlight/brightness`
  where `n` is some value between 0 and 255.

- https://github.com/timothyhollabaugh/pi-touchscreen-timeout

-----

# Usefull links #

- Kiosk mode
  - https://die-antwort.eu/techblog/2017-12-setup-raspberry-pi-for-kiosk-mode/
  - https://raspberrypi.stackexchange.com/questions/9215/how-to-set-up-kiosk-mode-in-raspbian
- [lsyncd - Default Config](https://axkibe.github.io/lsyncd/manual/config/layer4/)
  - [rsync](https://www.techonthenet.com/linux/commands/rsync.php)
